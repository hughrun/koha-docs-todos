# Koha Documentation Resources

* [Editing the Koha Manual](https://wiki.koha-community.org/wiki/Editing_the_Koha_Manual)
* [KohaManualTodo](https://annuel2.framapad.org/p/KohaManualTodo)

# Manual

* Clean up [Authorised Values](https://koha-community.org/manual/17.11/html/02_administration.html?highlight=authorised%20values#authorized-values)
  * LOC subvalues should be under LOC

# Guide

## Server Administration
1. Installation
2. Getting cron jobs set up
3. The SIP server

## Set up and configuration

### How Koha thinks (what things are called, item types as the circulation rule trigger etc)

### Managing collections: Item Types, Collection Codes and Locations

  1. How Koha wants you to structure your collections
  2. Alternative ways to structure your collections
  3. Locations - limitations and gotchas (PROC -> CART)
  4. Locations vs statuses

### Managing items: Item statuses, tracking

  1. (Mostly) manual item statuses
     1. Not for Loan
     2. Lost
     3. Damaged
     4. Withdrawn

  2. Automated item statuses
     1. transfers and in-transit
     2. INPRO - in-process as a location _and_ a status
     3. CART - the Cart location
     4. INPRO and CART together: useful, but also dangerous

### Managing borrowers

  1. Setting up membership types and forms
     1. Intranet
     2. OPAC
  2. Restrictions, Flags, Notes, and Messages: which does what?
     1. Notes
     2. Messages
     3. Restrictions
     4. Flags
        * Gone no address
        * Lost card
     5. Restrictions vs circulation limits

### Circulation rules

### Notices and Notifications
