This repo is just a place for me to put down my ideas for [Koha ILS](https://koha-community.org/) documentation. It's generally pretty good, but I think it could be restructured to be better. My comments below are not meant to be a complaint or criticism of those who have come before me, but rather an explanation of why and how I think they docs need to be improved. I'm keen to help.

# Current docs
Primarily I believe the Koha project can learn from many other open source projects and split the documentation into at least two sections: 'Reference' and 'Guides'. There may be an argument (see below) to split it further, with guides for general users vs administrators vs developers. The [current documentation](https://koha-community.org/documentation/) is pretty confusing for people not familiar with the project, and for end-users basically consists of the 'Manual'. This is good if you want to know what each setting or function in Koha does, but it's not particularly helpful if you're trying to build a picture of how to set up a new Koha installation to manage your library. It's reasonably unlikely a prospective user is going to be starting a library completely from scratch: documentation that doesn't explain how Koha manages general library concepts (e.g. item statuses generally, or various ways to block borrowers) is therefore inadequate. There's some stuff in the wiki, and some in mailing list archives, but that's not really a substitute for good documentation.

# Examples from other open source projects

## Meteor

Meteor divides documentation in three: _Tutorials_ for showing how to build in Meteor using particular modules or technologies, the _Guide_ to explain particular Meteor concepts, and the _Documentation_ which simply goes through every function and command in Meteor with a relatively terse description of all the options and the expected outcome.

* [Tutorials](https://www.meteor.com/tutorials)
* [Guide](https://guide.meteor.com/)
* [Documentation](http://docs.meteor.com/#/full/)

## Atom

Atom breaks the docs basically into the 'Flight Manual' (user guide) and the API reference for those wanting to get meta and code for Atom itself.

* [Flight Manual](http://flight-manual.atom.io/)
* [API Reference](https://atom.io/docs/api/v1.21.2/AtomEnvironment)

## Ghost

Ghost breaks documentation into four main areas. Ghost has end-users, system administrators and theme developers to think of (sometimes these may all be the same person e.g. me).

* [Help](https://help.ghost.org)
* [Documentation](https://docs.ghost.org/docs)
* [Themes](https://themes.ghost.org/docs)
* [API](https://api.ghost.org/)

## CiviCRM

CiviCRM breaks the documentation down by audience, and is probably a better equivalent to Koha than the previous examples:

* [User Guide](https://docs.civicrm.org/user/en/latest) - "For staff members who use CiviCRM's web-based interface as part of their job at an organization"
* [System Administrator Guide](https://docs.civicrm.org/sysadmin/en/latest) - "For tech savvy people who install, upgrade, and maintain CiviCRM for an organization"
* [Developer Guide](https://docs.civicrm.org/dev/en/latest) - "For computer programmers who create and improve functionality within CiviCRM"

# A possible new structure

Arguably, a _System Administrator Guide_ is currently completely missing in the Koha documentation. Several parts of the manual tell readers to 'speak to your system administrator' but there does not appear to be any docs aimed at said sysadmin. There are also various levels of 'Developer' - the skills and knowledge needed to create a theme are different to that required to build new functions in the core project, for example. So a possible structure could be:

* Guide
    * User Guide
    * System Administrator Guide
        - Server administration
        - Set up and configuration
    * Developer Guide
        - Core
        - Themes
        - Plugins
        - Documentation
        - Bugs and Enhancements

* Reference
    * Settings
    * API
    * File structure
    * cron jobs
