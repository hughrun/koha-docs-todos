# Notices and Notifications

Overdue notices have their own system.

Other notices and notifications use the notification settings in the borrower record.

Most are triggered straight away by the XXXX cron job.

Anything that is set to print ends up in the print queue.

Anything that fails in a way Koha recognises as a fail (i.e. not bounced emails) will go to notification list as FAILED and _does not join the print queue_.

gather_print_notices cron job collects all the various print notices and processes them: usually you want this to email them to you so you can print them out.